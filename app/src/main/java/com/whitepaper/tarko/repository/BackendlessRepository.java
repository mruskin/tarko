package com.whitepaper.tarko.repository;

import android.util.Log;

import com.backendless.Backendless;
import com.backendless.async.callback.AsyncCallback;
import com.backendless.exceptions.BackendlessFault;
import com.backendless.persistence.DataQueryBuilder;
import com.whitepaper.tarko.data.discounts;

import java.util.List;

public class BackendlessRepository {

    private List<discounts> listOfdiscounts;
    public static DataQueryBuilder queryBuilder;

    public void getDiscountsList() {
        queryBuilder = DataQueryBuilder.create();
        Backendless.Data.of(discounts.class).find(queryBuilder, new AsyncCallback<List<discounts>>() {
            @Override
            public void handleResponse(List<discounts> response) {
                Log.i("Success: ", response == null ? "response is null" : response.toString());
                listOfdiscounts = response;
            }

            @Override
            public void handleFault(BackendlessFault fault) {
                Log.e("ERROR: ", fault.toString());
            }
        });

    }

    public List<discounts> getListOfdiscounts() {
        return listOfdiscounts;
    }

    public void setListOfdiscounts(List<discounts> listOfdiscounts) {
        this.listOfdiscounts = listOfdiscounts;
    }
}

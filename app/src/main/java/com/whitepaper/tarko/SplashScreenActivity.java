package com.whitepaper.tarko;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.backendless.Backendless;
import com.backendless.async.callback.AsyncCallback;
import com.backendless.exceptions.BackendlessFault;
import com.backendless.persistence.DataQueryBuilder;
import com.whitepaper.tarko.config.BackendLessConfig;
import com.whitepaper.tarko.config.Constants;
import com.whitepaper.tarko.config.LocaleHelper;
import com.whitepaper.tarko.config.LocationHelper;
import com.whitepaper.tarko.data.campaigns;
import com.whitepaper.tarko.data.discounts;
import com.whitepaper.tarko.data.groups;
import com.whitepaper.tarko.data.networks;
import com.whitepaper.tarko.data.sql.DiscountEntity;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class SplashScreenActivity extends AppCompatActivity {

    private ArrayList<campaigns> listOfcampaigns;
    private HashSet<networks> uniquesSetOfNetworks;
    private HashSet<groups> uniquesSetOfGroups;
    public static DataQueryBuilder queryBuilder;
    private ArrayList<DiscountEntity> discountsCollection;

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(LocaleHelper.onAttach(base, Constants.ENGLISH_LANGUAGE));
        //language to context
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);

        if (checkInternet(getApplicationContext())) {
            //BACKENDLESS CONFIG
            Backendless.setUrl(BackendLessConfig.SERVER_URL);
            Backendless.initApp(getApplicationContext(), BackendLessConfig.APPLICATION_ID, BackendLessConfig.API_KEY);
            getDiscountsList();
        } else {
            LayoutInflater inflater = getLayoutInflater();
            View layout = inflater.inflate(R.layout.custom_toast,
                    findViewById(R.id.custom_toast_container));
            try {
                TimeUnit.SECONDS.sleep(2);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            TextView text = layout.findViewById(R.id.text);
            text.setText("No Internet Connection");

            Toast toast = new Toast(getApplicationContext());
            toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
            toast.setDuration(Toast.LENGTH_LONG);
            toast.setView(layout);
            toast.show();
            finishAndRemoveTask();
        }

    }

    public boolean checkInternet(Context context) {
        String status = null;
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        if (activeNetwork != null) {
            if (activeNetwork.getType() == ConnectivityManager.TYPE_WIFI) {
                status = "Wifi enabled";
                return true;
            } else if (activeNetwork.getType() == ConnectivityManager.TYPE_MOBILE) {
                status = "Mobile data enabled";
                return true;
            }
        } else {
            status = "No internet is available";
            return false;
        }
        return false;
    }

    public void getDiscountsList() {
        queryBuilder = DataQueryBuilder.create();
        queryBuilder.setWhereClause("end_date >= '" + new Date() + "'" + " AND city.title = '" + LocationHelper.getPersistedData(this, Constants.DEFAULT_LOCATION) + "'");


        Backendless.Data.of(campaigns.class).find(queryBuilder, new AsyncCallback<List<campaigns>>() {
            @Override
            public void handleResponse(List<campaigns> response) {
                Log.i("Success: ", response == null ? "response is null" : response.toString());
                listOfcampaigns = (ArrayList<campaigns>) response;
                uniquesSetOfNetworks = new HashSet<>();
                uniquesSetOfGroups = new HashSet<>();
                discountsCollection = new ArrayList<>();
                Intent intent = new Intent(getApplicationContext(),
                        MainFragmentActivity.class);
                intent.putExtra("campaigns", listOfcampaigns);
                for (campaigns campaign : listOfcampaigns) {
                    if (campaign.getDiscounts() != null) {
                        for (discounts discount : campaign.getDiscounts()) {
                            if (discount != null) {
                                DiscountEntity discountEntity = new DiscountEntity();
                                discountEntity.setWeight(discount.getWeight());
                                discountEntity.setVolume(discount.getVolume());
                                discountEntity.setTitle(discount.getTitle());
                                discountEntity.setRu_title(discount.getRu_title());
                                discountEntity.setObjectId(discount.getObjectId());
                                discountEntity.setImage(discount.getImage());
                                discountEntity.setEt_title(discount.getEt_title());
                                discountEntity.setGroupTitle(discount.getGroup().getTitle());
                                discountEntity.setStart_date(campaign.getStart_date());
                                discountEntity.setEnd_date(campaign.getEnd_date());
                                discountEntity.setNetworkTitle(campaign.getNetwork().getTitle());
                                discountEntity.setEn_title(discount.getEn_title());
                                discountEntity.setRegularPrice(discount.getRegularPrice());
                                discountEntity.setCampaignTitle(campaign.getTitle());
                                discountEntity.setDiscountPrice(discount.getDiscountPrice());
                                discountEntity.setNetworkImage(campaign.getNetwork().getImage());
                                discountEntity.setGroupEtTitle(discount.getGroup().getEt_title());
                                discountEntity.setGroupRuitle(discount.getGroup().getRu_title());
                                discountEntity.setCampaignRuTitle(campaign.getRu_title());
                                discountEntity.setCampaignEtTitle(campaign.getEt_title());
                                uniquesSetOfGroups.add(discount.getGroup());
                                discountsCollection.add(discountEntity);
                            }
                        }
                    }
                    uniquesSetOfNetworks.add(campaign.getNetwork());
                }
                intent.putExtra("uniquesSetOfNetworks", uniquesSetOfNetworks);
                intent.putExtra("uniquesSetOfGroups", uniquesSetOfGroups);
                intent.putExtra("discounts", discountsCollection);
                startActivity(intent);
                finish();
            }

            @Override
            public void handleFault(BackendlessFault fault) {
                Log.e("ERROR: ", fault.toString());
            }
        });

    }
}

package com.whitepaper.tarko.config;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class Constants {

    public static final String EURO = "\u20ac";
    public static final String SINGLE_GOOD_FRAGMENT_TAG = "singleGood";
    public static final String ERROR_FRAGMENT_TAG = "errorFragment";
    public static final String DISCOUNTS_BY_SHOP_FRAGMENT_TAG = "discountsByShopFragment";
    public static final String DISCOUNTS_BY_CATEGORY_FRAGMENT_TAG = "discountsByCategoryFragment";
    public static final String ERROR_MESSAGE = "errorMessage";
    public static final String LANGUAGES_FRAGMENT_TAG = "languagesFragment";
    public static final String LOCATION_FRAGMENT_TAG = "locationFragment";
    public static final String MAIN_MENU_DISCOUNTS_FRAGMENT_TAG = "discountsFragment";
    public static final String MAIN_MENU_CART_FRAGMENT_TAG = "cartFragment";
    public static final String MAIN_MENU_NETWORKS_FRAGMENT_TAG = "networksFragment";
    public static final String MAIN_MENU_GROUPS_FRAGMENT_TAG = "groupsFragment";
    public static final String MAIN_MENU_SETTINGS_FRAGMENT_TAG = "settingsFragment";
    public static final int SPAN_COUNT_FOR_NETWORK_FRAGMENT = 3;
    public static final String SHARED_PREFERENCES_SELECTED_LANGUAGE = "sharedPrefsLanguage";
    public static final String SHARED_PREFERENCES_ITEMS_IN_CART = "sharedPrefsLanguage";
    public static final String SHARED_PREFERENCES_LANGUAGE_KEY = "lang";
    public static final String SHARED_PREFERENCES_LOCATION_KEY = "location";
    public static final String SHARED_PREFERENCES_DISCOUNT_ID = "objectId";
    public static final String ESTONIAN_LANGUAGE = "et";
    public static final String ENGLISH_LANGUAGE = "en";
    public static final String RUSSIAN_LANGUAGE = "ru";
    public static final String DEFAULT_LOCATION = "Tallinn";
    public static final String SHARED_PREFERENCES_SELECTED_LOCATION = "Tallinn";
    public static final String LOCATION_TALLINN = "Tallinn";
    public static final String LOCATION_RIGA = "Riga";
    public static final String LOCATION_VILNIUS = "Vilnius";
    public static final String LOCATION_TARTU = "Tartu";
    public static final List<String> listOfFragmentsToRemove =
            Collections.unmodifiableList(Arrays.asList(ERROR_FRAGMENT_TAG, LANGUAGES_FRAGMENT_TAG, SINGLE_GOOD_FRAGMENT_TAG, DISCOUNTS_BY_SHOP_FRAGMENT_TAG, DISCOUNTS_BY_CATEGORY_FRAGMENT_TAG));
}

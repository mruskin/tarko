package com.whitepaper.tarko.config;

import android.content.Context;
import android.content.SharedPreferences;

import com.whitepaper.tarko.data.discounts;

public class CartDataManager {

    public static String getItemsInCart(Context context) {
        SharedPreferences sharedPref = context.getSharedPreferences(
                Constants.SHARED_PREFERENCES_ITEMS_IN_CART, Context.MODE_PRIVATE);
        String listOfValues = sharedPref.getString(Constants.SHARED_PREFERENCES_DISCOUNT_ID, "");
        if (listOfValues != null && listOfValues.endsWith(",")) {
            listOfValues = listOfValues.substring(0, listOfValues.length() - 1);
        }

        return listOfValues;
    }

    public static void saveGoodToSharedPrefs(Context context, discounts singleGood) {
        SharedPreferences sharedPref = context.getSharedPreferences(Constants.SHARED_PREFERENCES_ITEMS_IN_CART, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString(Constants.SHARED_PREFERENCES_DISCOUNT_ID, "'" + singleGood.getObjectId() + "'" + "," + getItemsInCart(context));
        editor.apply();
    }

    public static void removeGoodFromSharedPrefs(Context context, discounts singleGood) {
        SharedPreferences sharedPref = context.getSharedPreferences(Constants.SHARED_PREFERENCES_ITEMS_IN_CART, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        String result = getItemsInCart(context).replace("'" + singleGood.getObjectId() + "'", "");
        if (result != null && result.startsWith(",")) {
            result = result.substring(1);
        }
        editor.putString(Constants.SHARED_PREFERENCES_DISCOUNT_ID, result);
        editor.apply();
    }
}

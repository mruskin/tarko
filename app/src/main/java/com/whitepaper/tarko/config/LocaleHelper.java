package com.whitepaper.tarko.config;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Build;

import java.util.Locale;

public class LocaleHelper {

    public static Context onAttach(Context context) {
        String lang = getPersistedData(context, Locale.getDefault().getLanguage());
        return setLocale(context, lang);
    }

    public static Context onAttach(Context context, String defaultLang) {
        String lang = getPersistedData(context, defaultLang);
        return setLocale(context, lang);
    }

    public static Context setLocale(Context context, String language) {
        persist(context, language);
        Locale locale = new Locale(language);
        Locale.setDefault(locale);
        Resources res = context.getResources();
        Configuration config = new Configuration(res.getConfiguration());
        if (Build.VERSION.SDK_INT >= 17) {
            config.setLocale(locale);
            context = context.createConfigurationContext(config);
        } else {
            config.locale = locale;
            res.updateConfiguration(config, res.getDisplayMetrics());
        }
        return context;
    }

//    public static Context setLocale(Context context, String lang) {
//        persist(context, lang);
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
//            return updateResources(context, lang);
//        }
//        return updateResourcesLegacy(context, lang);
//    }
//
//    @TargetApi(Build.VERSION_CODES.N)
//    private static Context updateResources(Context context, String lang) {
//        Locale locale = new Locale(lang);
//        Locale.setDefault(locale);
//        Configuration conf = context.getResources().getConfiguration();
//        conf.setLocale(locale);
//        conf.setLayoutDirection(locale);
//        return context.createConfigurationContext(conf);
//    }

//    @SuppressWarnings("deprecation")
//    private static Context updateResourcesLegacy(Context context, String lang) {
//        Locale locale = new Locale(lang);
//        Locale.setDefault(locale);
//
//        Resources resources = context.getResources();
//        Configuration configuration = resources.getConfiguration();
//        configuration.locale = locale;
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
//            configuration.setLayoutDirection(locale);
//        }
//        resources.updateConfiguration(configuration, resources.getDisplayMetrics());
//        return context;
//    }

    private static void persist(Context context, String lang) {
        SharedPreferences sharedPref = context.getSharedPreferences(Constants.SHARED_PREFERENCES_SELECTED_LANGUAGE, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString(Constants.SHARED_PREFERENCES_LANGUAGE_KEY, lang);
        editor.apply();
    }

    public static String getPersistedData(Context context, String language) {
        SharedPreferences sharedPref = context.getSharedPreferences(
                Constants.SHARED_PREFERENCES_SELECTED_LANGUAGE, Context.MODE_PRIVATE);
        return sharedPref.getString(Constants.SHARED_PREFERENCES_LANGUAGE_KEY, language);
    }
}

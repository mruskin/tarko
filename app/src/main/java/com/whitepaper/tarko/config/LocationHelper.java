package com.whitepaper.tarko.config;

import android.content.Context;
import android.content.SharedPreferences;

public class LocationHelper {

    public static void saveLocation(Context context, String location) {
        SharedPreferences sharedPref = context.getSharedPreferences(Constants.SHARED_PREFERENCES_SELECTED_LOCATION, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString(Constants.SHARED_PREFERENCES_LOCATION_KEY, location);
        editor.apply();
    }

    public static String getPersistedData(Context context, String location) {
        SharedPreferences sharedPref = context.getSharedPreferences(
                Constants.SHARED_PREFERENCES_SELECTED_LOCATION, Context.MODE_PRIVATE);
        return sharedPref.getString(Constants.SHARED_PREFERENCES_LOCATION_KEY, location);
    }
}

package com.whitepaper.tarko.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.preference.Preference;
import android.support.v7.preference.PreferenceFragmentCompat;
import android.widget.Toast;

import com.whitepaper.tarko.R;
import com.whitepaper.tarko.config.Constants;

public class SettingsFragment extends PreferenceFragmentCompat {

    private FragmentManager manager;

    @Override
    public void onCreatePreferences(Bundle bundle, String s) {
        addPreferencesFromResource(R.xml.preferences);

        Preference languagePreference = findPreference("language");
        Preference feedbackPreference = findPreference("feedback");
//        Preference locationPreference = findPreference("location");

        languagePreference.setOnPreferenceClickListener(preference -> {
            manager = getFragmentManager();
            FragmentTransaction fragmentTransaction = manager.beginTransaction();
            fragmentTransaction.replace(R.id.main_frame, new LanguagesFragment(), Constants.LANGUAGES_FRAGMENT_TAG);
            fragmentTransaction.addToBackStack(null);
            fragmentTransaction.commit();
            return false;
        });

        feedbackPreference.setOnPreferenceClickListener(preference -> {
            Intent i = new Intent(Intent.ACTION_SEND);
            i.setType("message/rfc822");
            i.putExtra(Intent.EXTRA_EMAIL, new String[]{"info@whitepaper.email"});
            i.putExtra(Intent.EXTRA_SUBJECT, "android - message from user");
            i.putExtra(Intent.EXTRA_TEXT, "");
            try {
                startActivity(Intent.createChooser(i, "Send mail..."));
            } catch (android.content.ActivityNotFoundException ex) {
                Toast.makeText(getContext(), "There are no email clients installed.", Toast.LENGTH_LONG).show();
            }


//            manager = getFragmentManager();
//            FragmentTransaction fragmentTransaction = manager.beginTransaction();
//            fragmentTransaction.replace(R.id.main_frame, new CItyFragment(), Constants.LOCATION_FRAGMENT_TAG);
//            fragmentTransaction.addToBackStack(null);
//            fragmentTransaction.commit();
            return false;
        });
    }


}

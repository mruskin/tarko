package com.whitepaper.tarko.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.whitepaper.tarko.R;
import com.whitepaper.tarko.SplashScreenActivity;
import com.whitepaper.tarko.config.Constants;
import com.whitepaper.tarko.config.LocationHelper;

public class CItyFragment extends Fragment {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.city_fragment, container, false);
        Button locTlnTV = rootView.findViewById(R.id.locTlnTV);
        Button locTartuTV = rootView.findViewById(R.id.locTartuTV);
        Button locRigaTV = rootView.findViewById(R.id.locRigaTV);
        Button locVilniusTV = rootView.findViewById(R.id.locVilniusTV);

        locTlnTV.setOnClickListener(v -> {
            LocationHelper.saveLocation(getContext(), Constants.LOCATION_TALLINN);
            Intent intent = new Intent(getContext(), SplashScreenActivity.class);
            getActivity().finish();
            startActivity(intent);
        });

        locTartuTV.setOnClickListener(v -> {
            LocationHelper.saveLocation(getContext(), Constants.LOCATION_TARTU);
            Intent intent = new Intent(getContext(), SplashScreenActivity.class);
            getActivity().finish();
            startActivity(intent);
        });

        locRigaTV.setOnClickListener(v -> {
            LocationHelper.saveLocation(getContext(), Constants.LOCATION_RIGA);
            Intent intent = getActivity().getIntent();
            getActivity().finish();
            startActivity(intent);
        });

        locVilniusTV.setOnClickListener(v -> {
            LocationHelper.saveLocation(getContext(), Constants.LOCATION_VILNIUS);
            Intent intent = getActivity().getIntent();
            getActivity().finish();
            startActivity(intent);
        });

        return rootView;
    }

}

package com.whitepaper.tarko.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.backendless.persistence.DataQueryBuilder;
import com.whitepaper.tarko.R;
import com.whitepaper.tarko.adapters.GroupsViewAdapter;
import com.whitepaper.tarko.adapters.GroupsViewAdapterList;
import com.whitepaper.tarko.config.Constants;
import com.whitepaper.tarko.data.campaigns;
import com.whitepaper.tarko.data.groups;
import com.whitepaper.tarko.data.sql.DiscountEntity;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class GroupsFragment extends Fragment implements GroupsViewAdapter.OnGroupsListener, FragmentManager.OnBackStackChangedListener, GroupsViewAdapterList.OnGroupsListener {

    private List<groups> groupsList;
    private List<DiscountEntity> discountsList;
    private List<campaigns> campaignsList;
    public static DataQueryBuilder queryBuilder;
    private RecyclerView recyclerView;
    private RecyclerView.Adapter adapter;
    private FragmentManager manager;
    private RecyclerView.LayoutManager layoutManager;

    public GroupsFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_groups, container, false);
        recyclerView = rootView.findViewById(R.id.groupItems);

//        View rootView = inflater.inflate(R.layout.groups_data, container, false);
        manager = getFragmentManager();
        manager.addOnBackStackChangedListener(this);
//        recyclerView = rootView.findViewById(R.id.groupsItems);
        layoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(layoutManager);
//        adapter = new RecyclerViewAdapter(getContext(), groupsList, this);
        groupsList.sort(Comparator.comparing(groups::getTitle));
        adapter = new GroupsViewAdapterList(getContext(), groupsList, this);
        recyclerView.setAdapter(adapter);
        return rootView;
    }

    @Override
    public void onGroupsClick(int position) {
        final SingleCategoryFragment fragment = new SingleCategoryFragment();
        groups item = groupsList.get(position);
        List<DiscountEntity> listByGroup = new ArrayList<>();

        FragmentTransaction fragmentTransaction = manager.beginTransaction();
        for (DiscountEntity discount : discountsList) {
            if (discount.getGroupTitle().equals(item.getTitle())) {
                listByGroup.add(discount);
            }
        }

        fragment.setDiscountsListByCategory(listByGroup);
        fragmentTransaction.replace(R.id.main_frame, fragment, Constants.DISCOUNTS_BY_CATEGORY_FRAGMENT_TAG);
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
    }

    public void setGroupsList(List<groups> groupsList) {
        this.groupsList = groupsList;
    }

    public void setDiscountsList(List<DiscountEntity> discountsList) {
        this.discountsList = discountsList;
    }

    @Override
    public void onBackStackChanged() {

    }

    public void setCampaignsList(List<campaigns> campaignsList) {
        this.campaignsList = campaignsList;
    }

}

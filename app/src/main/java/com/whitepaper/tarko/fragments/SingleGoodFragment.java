package com.whitepaper.tarko.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;
import com.whitepaper.tarko.R;
import com.whitepaper.tarko.config.Constants;
import com.whitepaper.tarko.config.LocaleHelper;
import com.whitepaper.tarko.config.database.AppDatabase;
import com.whitepaper.tarko.config.database.AppExecutors;
import com.whitepaper.tarko.data.sql.DiscountEntity;

import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.Locale;

public class SingleGoodFragment extends Fragment {

    private AppDatabase mDb;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        setHasOptionsMenu(true);

        View rootView = inflater.inflate(R.layout.fragment_good, container, false);
        Bundle singleGoodBundle = getArguments();
        DiscountEntity singleGood = (DiscountEntity) singleGoodBundle.getSerializable("object");
        String selectedFragment = singleGoodBundle.getString("fragmentFrom");
        ImageView sgImage = rootView.findViewById(R.id.sgImage);
        TextView sgTitle = rootView.findViewById(R.id.sgTitle);
        TextView sgDescription = rootView.findViewById(R.id.sgDescription);
        TextView sgRegularPrice = rootView.findViewById(R.id.sgRegularPrice);
        TextView sgDiscountPrice = rootView.findViewById(R.id.sgDiscountPrice);
        TextView sgStore = rootView.findViewById(R.id.sgStore);
        TextView sgCampaign = rootView.findViewById(R.id.sgCampaign);
        TextView sgDeadline = rootView.findViewById(R.id.sgDeadline);
        TextView sgTotalDiscount = rootView.findViewById(R.id.sgTotalDiscount);
        if (singleGood.getImage() == null) {
            sgImage.setImageResource(R.drawable.image_not_found);
        } else {
            Picasso.get().load(singleGood.getImage()).into(sgImage);
        }


        sgRegularPrice.append(" " + singleGood.getRegularPrice() + " " + Constants.EURO);
        sgDiscountPrice.append(" " + singleGood.getDiscountPrice() + " " + Constants.EURO);
        sgStore.append(" " + singleGood.getNetworkTitle());

        String lang = LocaleHelper.getPersistedData(getContext(), Locale.getDefault().getLanguage());
        if (Constants.ESTONIAN_LANGUAGE.equals(lang)) {
            sgCampaign.append(" " + singleGood.getCampaignEtTitle());
            sgTitle.setText(singleGood.getEt_title());
        } else if (Constants.RUSSIAN_LANGUAGE.equals(lang)) {
            sgCampaign.append(" " + singleGood.getCampaignRuTitle());
            sgTitle.setText(singleGood.getRu_title());
        } else {
            sgCampaign.append(" " + singleGood.getCampaignTitle());
            sgTitle.setText(singleGood.getTitle());
        }

        sgTotalDiscount.append(" " + singleGood.getDiscountPercent() + " %");

        Format formatter = new SimpleDateFormat("EEE, dd MMM");
        String endDate = formatter.format(singleGood.getEnd_date());
        sgDeadline.append(" " + endDate);

        Button btnAddToCart = rootView.findViewById(R.id.btnAddToCart);


        btnAddToCart.setOnClickListener(v -> {
            AppExecutors.getInstance().diskIO().execute(() -> {
                DiscountEntity discountEntity = new DiscountEntity();
                discountEntity.setEn_title(singleGood.getEn_title());
                discountEntity.setCampaignTitle(singleGood.getCampaignTitle());
                discountEntity.setCreated(singleGood.getCreated());
                discountEntity.setDiscountPrice(singleGood.getDiscountPrice());
                discountEntity.setEt_title(singleGood.getEt_title());
//                    discountEntity.setGroup(singleGood.getGroup());
                discountEntity.setImage(singleGood.getImage());
                discountEntity.setGroupTitle(singleGood.getGroupTitle());
                discountEntity.setObjectId(singleGood.getObjectId());
                discountEntity.setRegularPrice(singleGood.getRegularPrice());
                discountEntity.setRu_title(singleGood.getRu_title());
                discountEntity.setTitle(singleGood.getTitle());
                discountEntity.setVolume(singleGood.getVolume());
                discountEntity.setWeight(singleGood.getWeight());
                discountEntity.setObjectId(singleGood.getObjectId());
                discountEntity.setStart_date(singleGood.getStart_date());
                discountEntity.setEnd_date(singleGood.getEnd_date());
                discountEntity.setNetworkImage(singleGood.getNetworkImage());
                discountEntity.setCampaignEtTitle(singleGood.getCampaignEtTitle());
                discountEntity.setCampaignRuTitle(singleGood.getCampaignRuTitle());
                discountEntity.setGroupEtTitle(singleGood.getGroupEtTitle());
                discountEntity.setGroupRuitle(singleGood.getGroupRuitle());
                mDb = AppDatabase.getInstance(getActivity().getApplication().getApplicationContext());
                mDb.discountDao().insertDiscount(discountEntity);
            });
//            CartDataManager.saveGoodToSharedPrefs(getActivity(), singleGood);
            Toast.makeText(getActivity(), getString(R.string.itemAddedToCart), Toast.LENGTH_SHORT).show();
        });

        if (selectedFragment != null && selectedFragment.equals(Constants.MAIN_MENU_CART_FRAGMENT_TAG)) {
            btnAddToCart.setVisibility(View.GONE);
        }

        return rootView;
    }

}

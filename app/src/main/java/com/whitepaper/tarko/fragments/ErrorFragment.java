package com.whitepaper.tarko.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.whitepaper.tarko.R;
import com.whitepaper.tarko.config.Constants;

public class ErrorFragment extends Fragment {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.error_fragment, container, false);
        Bundle bundle = getArguments();
        String errorMessage = bundle.getString(Constants.ERROR_MESSAGE);
        TextView errorTextView = rootView.findViewById(R.id.errorMessage);
        ImageView errorImage = rootView.findViewById(R.id.errorImage);
        errorTextView.setText(errorMessage);
        if (errorMessage.equals(getString(R.string.emptyCartError))) {
            Picasso.get().load(R.drawable.cart_is_empty).into(errorImage);
        }
        return rootView;
    }
}

package com.whitepaper.tarko.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.whitepaper.tarko.R;
import com.whitepaper.tarko.adapters.RecyclerViewAdapter;
import com.whitepaper.tarko.config.Constants;
import com.whitepaper.tarko.data.sql.DiscountEntity;

import java.util.Comparator;
import java.util.List;

public class GoodsByShopFragment extends Fragment implements RecyclerViewAdapter.OnGoodsListener, FragmentManager.OnBackStackChangedListener {

    private static List<DiscountEntity> discoutsListByShop;
    private RecyclerView recyclerView;
    private RecyclerView.Adapter adapter;
    private RecyclerView.LayoutManager layoutManager;
    private FragmentManager manager;

    public GoodsByShopFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_goods_list, container, false);
        manager = getFragmentManager();
        manager.addOnBackStackChangedListener(this);
        recyclerView = rootView.findViewById(R.id.listItems);
        layoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(layoutManager);
        discoutsListByShop.sort(Comparator.comparingLong(DiscountEntity::getDiscountPercent).reversed());
        adapter = new RecyclerViewAdapter(getContext(), discoutsListByShop, this);
        recyclerView.setAdapter(adapter);
        return rootView;
    }

    public static List<DiscountEntity> getDiscoutsListByShop() {
        return discoutsListByShop;
    }

    public static void setDiscoutsListByShop(List<DiscountEntity> discoutsListByShop) {
        GoodsByShopFragment.discoutsListByShop = discoutsListByShop;
    }

    @Override
    public void onGoodsClick(int position) {
        SingleGoodFragment fragment = new SingleGoodFragment();
        Bundle b = new Bundle();
        DiscountEntity item = discoutsListByShop.get(position);
        b.putSerializable("object", item);
        fragment.setArguments(b);
        FragmentTransaction fragmentTransaction = getActivity().getSupportFragmentManager().beginTransaction();
        fragmentTransaction.add(R.id.main_frame, fragment, Constants.SINGLE_GOOD_FRAGMENT_TAG);
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
    }

    @Override
    public void onBackStackChanged() {

    }
}

package com.whitepaper.tarko.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.whitepaper.tarko.R;
import com.whitepaper.tarko.config.Constants;
import com.whitepaper.tarko.config.LocaleHelper;

public class LanguagesFragment extends Fragment {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.languages_fragment, container, false);
        Button estBtn = rootView.findViewById(R.id.estonianBtn);
        Button engBtn = rootView.findViewById(R.id.englishBtn);
        Button rusBtn = rootView.findViewById(R.id.russianBtn);

        estBtn.setOnClickListener(v -> {
            LocaleHelper.setLocale(getContext(), Constants.ESTONIAN_LANGUAGE);
            Intent intent = getActivity().getIntent();
            getActivity().finish();
            startActivity(intent);
        });

        engBtn.setOnClickListener(v -> {
            LocaleHelper.setLocale(getContext(), Constants.ENGLISH_LANGUAGE);
            Intent intent = getActivity().getIntent();
            getActivity().finish();
            startActivity(intent);
        });

        rusBtn.setOnClickListener(v -> {
            LocaleHelper.setLocale(getContext(), Constants.RUSSIAN_LANGUAGE);
            Intent intent = getActivity().getIntent();
            getActivity().finish();
            startActivity(intent);
        });
        return rootView;
    }
}

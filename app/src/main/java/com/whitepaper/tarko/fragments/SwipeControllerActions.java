package com.whitepaper.tarko.fragments;

public abstract class SwipeControllerActions {

    public void onLeftClicked(int position) {
    }

    public void onRightClicked(int position) {
    }

}

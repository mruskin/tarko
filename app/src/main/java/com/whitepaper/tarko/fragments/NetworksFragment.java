package com.whitepaper.tarko.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.backendless.persistence.DataQueryBuilder;
import com.whitepaper.tarko.R;
import com.whitepaper.tarko.adapters.NetworksViewAdapter;
import com.whitepaper.tarko.config.Constants;
import com.whitepaper.tarko.data.campaigns;
import com.whitepaper.tarko.data.networks;
import com.whitepaper.tarko.data.sql.DiscountEntity;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class NetworksFragment extends Fragment implements NetworksViewAdapter.OnNetworksListener, FragmentManager.OnBackStackChangedListener {

    List<networks> networksList;
    List<DiscountEntity> discountsList;
    List<campaigns> campaignsList;
    public static DataQueryBuilder queryBuilder;
    private RecyclerView recyclerView;
    private RecyclerView.Adapter adapter;
    private FragmentManager manager;

    public NetworksFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.row_data, container, false);
        manager = getFragmentManager();
        recyclerView = rootView.findViewById(R.id.networksItems);
        recyclerView.setLayoutManager(new GridLayoutManager(getActivity(), Constants.SPAN_COUNT_FOR_NETWORK_FRAGMENT));
        adapter = new NetworksViewAdapter(getContext(), networksList, this);
        manager.addOnBackStackChangedListener(this);
        recyclerView.setAdapter(adapter);
        return rootView;
    }

    public List<networks> getNetworksList() {
        return networksList;
    }

    public void setNetworksList(List<networks> networksList) {
        this.networksList = networksList;
    }

    public void setDiscountsList(List<DiscountEntity> discountsList) {
        this.discountsList = discountsList;
    }

    public void setCampaignsList(List<campaigns> campaignsList) {
        this.campaignsList = campaignsList;
    }

    @Override
    public void onNetworksClick(int position) {
        final GoodsByShopFragment fragment = new GoodsByShopFragment();
        networks item = networksList.get(position);

        List<DiscountEntity> listByNetwork = new ArrayList<>();

        for (DiscountEntity discount : discountsList) {
            if (discount.getNetworkTitle().equals(item.getTitle())) {
                listByNetwork.add(discount);
            }
        }

//        for (discounts discount : discountsList) {
//            if (discount.getCampaign().getNetwork().getTitle().equals(item.getTitle())) {
//                listByNetwork.add(discount);
//            }
//        }

        FragmentTransaction fragmentTransaction = manager.beginTransaction();
        fragment.setDiscoutsListByShop(listByNetwork);
        fragmentTransaction.replace(R.id.main_frame, fragment, Constants.DISCOUNTS_BY_SHOP_FRAGMENT_TAG);
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
    }

    @Override
    public void onBackStackChanged() {

    }
}

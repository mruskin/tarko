package com.whitepaper.tarko.fragments;

import android.graphics.Canvas;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.backendless.persistence.DataQueryBuilder;
import com.whitepaper.tarko.R;
import com.whitepaper.tarko.adapters.CartRecyclerViewAdapter;
import com.whitepaper.tarko.config.Constants;
import com.whitepaper.tarko.config.database.AppDatabase;
import com.whitepaper.tarko.config.database.AppExecutors;
import com.whitepaper.tarko.data.sql.DiscountEntity;

import java.util.Comparator;
import java.util.List;

public class CartFragment extends Fragment implements CartRecyclerViewAdapter.OnGoodsListener {

    private RecyclerView recyclerView;
    private RecyclerView.Adapter adapter;
    private RecyclerView.LayoutManager layoutManager;
    private List<DiscountEntity> cartDiscoiuntsList;
    private FragmentManager manager;
    public static DataQueryBuilder queryBuilder;
    private ErrorFragment errorFragment;
    private AppDatabase mDb;
    SwipeController swipeController = null;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mDb = AppDatabase.getInstance(getActivity().getApplicationContext());
        return getView(inflater, container);
    }

    private void setupRecyclerView(RecyclerView recyclerView, CartRecyclerViewAdapter mAdapter) {

        swipeController = new SwipeController(new SwipeControllerActions() {
            @Override
            public void onRightClicked(int position) {
                AppExecutors.getInstance().diskIO().execute(() -> mDb.discountDao().deleteDiscount(getCartDiscoiuntsList().get(position)));
                Toast.makeText(getActivity(), "Item was removed from cart", Toast.LENGTH_SHORT).show();
                mAdapter.getDataSet().remove(position);
                mAdapter.notifyItemRemoved(position);
                mAdapter.notifyItemRangeChanged(position, mAdapter.getItemCount());
                if (mAdapter.getDataSet() == null || mAdapter.getDataSet().isEmpty()) {
                    FragmentTransaction fragmentTransaction = getActivity().getSupportFragmentManager().beginTransaction();
                    Bundle b = new Bundle();
                    b.putString(Constants.ERROR_MESSAGE, getResources().getString(R.string.emptyCartError));
                    errorFragment.setArguments(b);
                    fragmentTransaction.replace(R.id.main_frame, errorFragment, Constants.ERROR_FRAGMENT_TAG);
                    fragmentTransaction.commit();
                }
            }
        });

        ItemTouchHelper itemTouchhelper = new ItemTouchHelper(swipeController);
        itemTouchhelper.attachToRecyclerView(recyclerView);

        recyclerView.addItemDecoration(new RecyclerView.ItemDecoration() {
            @Override
            public void onDraw(Canvas c, RecyclerView parent, RecyclerView.State state) {
                swipeController.onDraw(c);
            }
        });
    }


    private View getView(LayoutInflater inflater, ViewGroup container) {
        View rootView = inflater.inflate(R.layout.fragment_goods_list, container, false);
        recyclerView = rootView.findViewById(R.id.listItems);


        manager = getFragmentManager();
        layoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(layoutManager);
        errorFragment = new ErrorFragment();
        try {
            getCartDiscoiuntsList().sort(Comparator.comparingLong(DiscountEntity::getDiscountPercent).reversed());
        } catch (Exception e) {
            Log.e("Empty cart: ", e.toString());
            Bundle b = new Bundle();
            b.putString(Constants.ERROR_MESSAGE, getResources().getString(R.string.emptyCartError));
            errorFragment.setArguments(b);
            FragmentTransaction fragmentTransaction = getActivity().getSupportFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.main_frame, errorFragment, Constants.ERROR_FRAGMENT_TAG);
            fragmentTransaction.commit();
        }

        adapter = new CartRecyclerViewAdapter(getContext(), getCartDiscoiuntsList(), this);
        setupRecyclerView(recyclerView, (CartRecyclerViewAdapter) adapter);

        ItemTouchHelper itemTouchhelper = new ItemTouchHelper(swipeController);
        itemTouchhelper.attachToRecyclerView(recyclerView);
        recyclerView.setAdapter(adapter);
        return rootView;
    }

    public void setCartDiscoiuntsList(List<DiscountEntity> cartDiscoiuntsList) {
        this.cartDiscoiuntsList = cartDiscoiuntsList;
    }

    public List<DiscountEntity> getCartDiscoiuntsList() {
        return cartDiscoiuntsList;
    }

    @Override
    public void onGoodsClick(int position) {
        SingleGoodFragment fragment = new SingleGoodFragment();
        Bundle b = new Bundle();
        DiscountEntity item = getCartDiscoiuntsList().get(position);
        b.putSerializable("object", item);
        b.putString("fragmentFrom", Constants.MAIN_MENU_CART_FRAGMENT_TAG);
        fragment.setArguments(b);
        FragmentTransaction fragmentTransaction = getActivity().getSupportFragmentManager().beginTransaction();
        fragmentTransaction.add(R.id.main_frame, fragment, Constants.SINGLE_GOOD_FRAGMENT_TAG);
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
    }

//    @Override
//    public void onDeleteClick(int position) {
//        AppExecutors.getInstance().diskIO().execute(() -> mDb.discountDao().deleteDiscount(getCartDiscoiuntsList().get(position)));
//        Toast.makeText(getActivity(), "Item was removed from cart", Toast.LENGTH_SHORT).show();
//        FragmentTransaction fragmentTransaction = getActivity().getSupportFragmentManager().beginTransaction();
//        CartFragment cartFragment = new CartFragment();
//        if (getCartDiscoiuntsList() != null && !getCartDiscoiuntsList().isEmpty()) {
//            fragmentTransaction.replace(R.id.main_frame, cartFragment, Constants.MAIN_MENU_CART_FRAGMENT_TAG);
//            fragmentTransaction.commit();
//        } else {
//            Bundle b = new Bundle();
//            b.putString(Constants.ERROR_MESSAGE, getResources().getString(R.string.emptyCartError));
//            errorFragment.setArguments(b);
//            fragmentTransaction.replace(R.id.main_frame, errorFragment, Constants.ERROR_FRAGMENT_TAG);
//            fragmentTransaction.commit();
//        }
//
//
////        getActivity().recreate();
//    }

}

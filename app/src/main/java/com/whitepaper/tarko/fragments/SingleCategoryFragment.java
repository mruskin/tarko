package com.whitepaper.tarko.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.backendless.persistence.DataQueryBuilder;
import com.whitepaper.tarko.R;
import com.whitepaper.tarko.adapters.RecyclerViewAdapter;
import com.whitepaper.tarko.config.Constants;
import com.whitepaper.tarko.data.sql.DiscountEntity;

import java.util.Collection;
import java.util.Comparator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.stream.Collectors;

public class SingleCategoryFragment extends Fragment implements RecyclerViewAdapter.OnGoodsListener, FragmentManager.OnBackStackChangedListener {

    private RecyclerView recyclerView;
    private RecyclerView.Adapter adapter;
    private RecyclerView.LayoutManager layoutManager;
    private List<DiscountEntity> discountsListByCategory;
    private FragmentManager manager;
    public static DataQueryBuilder queryBuilder;

    public SingleCategoryFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return getView(inflater, container);
    }

    private View getView(LayoutInflater inflater, ViewGroup container) {
        View rootView = inflater.inflate(R.layout.fragment_goods_list, container, false);
        recyclerView = rootView.findViewById(R.id.listItems);
        manager = getFragmentManager();
        manager.addOnBackStackChangedListener(this);
        layoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(layoutManager);
        getDiscountsListByCategory().stream()
                .sorted(Comparator.comparingLong(DiscountEntity::getDiscountPercent).reversed())
                .collect(Collectors.groupingBy(DiscountEntity::getNetworkTitle, LinkedHashMap::new, Collectors.toList()))
                .values().stream()
                .flatMap(Collection::stream)
                .collect(Collectors.toList());
//        getDiscountsListByCategory().sort(Comparator.comparingLong(DiscountEntity::getDiscountPercent).reversed());
        adapter = new RecyclerViewAdapter(getContext(), getDiscountsListByCategory(), this);
        recyclerView.setAdapter(adapter);
        return rootView;
    }

    @Override
    public void onGoodsClick(int position) {
        SingleGoodFragment fragment = new SingleGoodFragment();
        Bundle b = new Bundle();
        DiscountEntity item = getDiscountsListByCategory().get(position);
        b.putSerializable("object", item);
        fragment.setArguments(b);
        FragmentTransaction fragmentTransaction = getActivity().getSupportFragmentManager().beginTransaction();
        fragmentTransaction.add(R.id.main_frame, fragment, Constants.SINGLE_GOOD_FRAGMENT_TAG);
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
    }

    public List<DiscountEntity> getDiscountsListByCategory() {
        return discountsListByCategory;
    }

    public void setDiscountsListByCategory(List<DiscountEntity> discountsListByCategory) {
        this.discountsListByCategory = discountsListByCategory;
    }

    @Override
    public void onBackStackChanged() {

    }
}

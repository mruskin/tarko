package com.whitepaper.tarko;

import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.widget.FrameLayout;

import com.backendless.persistence.DataQueryBuilder;
import com.whitepaper.tarko.config.Constants;
import com.whitepaper.tarko.config.LocaleHelper;
import com.whitepaper.tarko.config.database.AppDatabase;
import com.whitepaper.tarko.config.database.AppExecutors;
import com.whitepaper.tarko.data.campaigns;
import com.whitepaper.tarko.data.groups;
import com.whitepaper.tarko.data.sql.DiscountEntity;
import com.whitepaper.tarko.fragments.CartFragment;
import com.whitepaper.tarko.fragments.ErrorFragment;
import com.whitepaper.tarko.fragments.GoodsListFragment;
import com.whitepaper.tarko.fragments.GroupsFragment;
import com.whitepaper.tarko.fragments.NetworksFragment;
import com.whitepaper.tarko.fragments.SettingsFragment;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class MainFragmentActivity extends AppCompatActivity {

    private GroupsFragment groupsFragment;
    private NetworksFragment networksFragment;
    private GoodsListFragment listFragment;
    private SettingsFragment settingsFragment;
    private CartFragment cartFragment;
    private ErrorFragment errorFragment;
    private AppDatabase mDb;

    private static List<DiscountEntity> discountsCollection;
    private static List<campaigns> campaignsCollection;
    private static HashSet uniquesSetOfNetworks;
    private static Set<groups> uniquesSetOfGroups;
    public static DataQueryBuilder queryBuilder;
    private FragmentManager manager;
    private BottomNavigationView mainNav;

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(LocaleHelper.onAttach(base, Constants.ENGLISH_LANGUAGE));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.my_options_menu, menu);
        return true;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Toolbar myToolbar = (Toolbar) findViewById(R.id.my_toolbar);
        setSupportActionBar(myToolbar);

        setContentView(R.layout.activity_main_fragment);
        manager = getSupportFragmentManager();

        mainNav = findViewById(R.id.main_nav);
        FrameLayout mainFrame = findViewById(R.id.main_frame);
        groupsFragment = new GroupsFragment();
        networksFragment = new NetworksFragment();
        listFragment = new GoodsListFragment();
        settingsFragment = new SettingsFragment();
        cartFragment = new CartFragment();
        errorFragment = new ErrorFragment();
//        mDb = AppDatabase.getInstance(getApplicationContext());
        discountsCollection = (ArrayList) getIntent().getExtras().getSerializable("discounts");
        campaignsCollection = (ArrayList) getIntent().getExtras().getSerializable("campaigns");
        uniquesSetOfNetworks = (HashSet) getIntent().getExtras().getSerializable("uniquesSetOfNetworks");
        uniquesSetOfGroups = (HashSet) getIntent().getExtras().getSerializable("uniquesSetOfGroups");
        getGroupsList(groupsFragment);

        mainNav.setOnNavigationItemSelectedListener(menuItem -> {
                    switch (menuItem.getItemId()) {
//                        case R.id.nav_home:
//                            removeNotDestroyedFragments();
//                            getDiscountsList(listFragment);
//                            return true;
                        case R.id.nav_stores:
                            removeNotDestroyedFragments();
                            getNetworksList(networksFragment);
                            return true;
                        case R.id.nav_category:
                            removeNotDestroyedFragments();
                            getGroupsList(groupsFragment);
                            return true;
                        case R.id.nav_settings:
                            removeNotDestroyedFragments();
                            getSettingsFragment(settingsFragment);
                            return true;
                        case R.id.nav_cart:
                            removeNotDestroyedFragments();
                            getCartFragment(cartFragment);
                            return true;

                        default:
                            getGroupsList(groupsFragment);
                            return true;
                    }
                }
        );
    }

    private void getCartFragment(CartFragment cartFragment) {
        final FragmentTransaction fragmentTransaction = manager.beginTransaction();

        AppExecutors.getInstance().diskIO().execute(() -> {
            mDb = AppDatabase.getInstance(getApplicationContext());
            List<DiscountEntity> discountEntityList = mDb.discountDao().loadAllDiscountsByDate();
            Log.i("Success: ", discountEntityList == null ? "response is null" : "Cart data received");
            if (discountEntityList != null && !discountEntityList.isEmpty()) {
                cartFragment.setCartDiscoiuntsList(discountEntityList);
                fragmentTransaction.replace(R.id.main_frame, cartFragment, Constants.MAIN_MENU_CART_FRAGMENT_TAG);
                fragmentTransaction.commit();
            } else {
                Bundle b = new Bundle();
                b.putString(Constants.ERROR_MESSAGE, getResources().getString(R.string.emptyCartError));
                errorFragment.setArguments(b);
                fragmentTransaction.replace(R.id.main_frame, errorFragment, Constants.ERROR_FRAGMENT_TAG);
                fragmentTransaction.commit();
            }
        });
    }

    private void getSettingsFragment(SettingsFragment settingsFragment) {
        final FragmentTransaction fragmentTransaction = manager.beginTransaction();
        fragmentTransaction.replace(R.id.main_frame, settingsFragment, Constants.MAIN_MENU_SETTINGS_FRAGMENT_TAG).commit();
    }

    private void getDiscountsList(final GoodsListFragment listFragment) {
        removeNotDestroyedFragments();
        final FragmentTransaction fragmentTransaction = manager.beginTransaction();
        if (getGoodsListFragment() != null) {
            fragmentTransaction.show(getGoodsListFragment());
        } else {
            listFragment.setResultCollection(discountsCollection);
            fragmentTransaction.replace(R.id.main_frame, listFragment, Constants.MAIN_MENU_DISCOUNTS_FRAGMENT_TAG);
        }
        fragmentTransaction.commit();
    }

    private void getNetworksList(NetworksFragment networksFragment) {
        final FragmentTransaction fragmentTransaction = manager.beginTransaction();
        networksFragment.setNetworksList(new ArrayList<>(uniquesSetOfNetworks));
        networksFragment.setDiscountsList(discountsCollection);
        networksFragment.setCampaignsList(campaignsCollection);
        fragmentTransaction.replace(R.id.main_frame, networksFragment, Constants.MAIN_MENU_NETWORKS_FRAGMENT_TAG);
        fragmentTransaction.commit();
    }

    private void getGroupsList(GroupsFragment groupsFragment) {
        FragmentTransaction fragmentTransaction = manager.beginTransaction();
        groupsFragment.setGroupsList(new ArrayList<>(uniquesSetOfGroups));
        groupsFragment.setDiscountsList(discountsCollection);
        fragmentTransaction.replace(R.id.main_frame, groupsFragment, Constants.MAIN_MENU_GROUPS_FRAGMENT_TAG);
        fragmentTransaction.commit();
    }

    private GoodsListFragment getGoodsListFragment() {
        return (GoodsListFragment) manager.findFragmentByTag(Constants.MAIN_MENU_DISCOUNTS_FRAGMENT_TAG);
    }

    private void removeNotDestroyedFragments() {
        manager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
        for (String fragmentTag : Constants.listOfFragmentsToRemove) {
            Fragment fragment = manager.findFragmentByTag(fragmentTag);
            if (fragment != null) {
                FragmentTransaction fragmentTransaction = manager.beginTransaction();
                fragmentTransaction.remove(fragment).commit();
            }
        }
    }

}

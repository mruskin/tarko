
package com.whitepaper.tarko.data;

import com.backendless.Backendless;
import com.backendless.async.callback.AsyncCallback;
import com.backendless.persistence.DataQueryBuilder;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

public class groups implements Serializable
{
    private Date updated;
  private String ownerId;
    private String et_title;
    private String en_title;
  private String title;
    private Date created;
  private String objectId;
    private String ru_title;
  private String image;

    public Date getUpdated()
  {
    return updated;
  }

  public String getOwnerId()
  {
    return ownerId;
  }

    public String getEt_title() {
        return et_title;
    }

    public void setEt_title(String et_title) {
        this.et_title = et_title;
  }

    public String getEn_title() {
        return en_title;
    }

    public void setEn_title(String en_title) {
        this.en_title = en_title;
  }

  public String getTitle()
  {
    return title;
  }

  public void setTitle( String title )
  {
    this.title = title;
  }

    public Date getCreated()
  {
    return created;
  }

  public String getObjectId()
  {
    return objectId;
  }

    public String getRu_title() {
        return ru_title;
    }

    public void setRu_title(String ru_title) {
        this.ru_title = ru_title;
  }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
  }

                                                    
  public groups save()
  {
    return Backendless.Data.of( groups.class ).save( this );
  }

  public void saveAsync( AsyncCallback<groups> callback )
  {
    Backendless.Data.of( groups.class ).save( this, callback );
  }

  public Long remove()
  {
    return Backendless.Data.of( groups.class ).remove( this );
  }

  public void removeAsync( AsyncCallback<Long> callback )
  {
    Backendless.Data.of( groups.class ).remove( this, callback );
  }

  public static groups findById( String id )
  {
    return Backendless.Data.of( groups.class ).findById( id );
  }

  public static void findByIdAsync( String id, AsyncCallback<groups> callback )
  {
    Backendless.Data.of( groups.class ).findById( id, callback );
  }

  public static groups findFirst()
  {
    return Backendless.Data.of( groups.class ).findFirst();
  }

  public static void findFirstAsync( AsyncCallback<groups> callback )
  {
    Backendless.Data.of( groups.class ).findFirst( callback );
  }

  public static groups findLast()
  {
    return Backendless.Data.of( groups.class ).findLast();
  }

  public static void findLastAsync( AsyncCallback<groups> callback )
  {
    Backendless.Data.of( groups.class ).findLast( callback );
  }

  public static List<groups> find( DataQueryBuilder queryBuilder )
  {
    return Backendless.Data.of( groups.class ).find( queryBuilder );
  }

  public static void findAsync( DataQueryBuilder queryBuilder, AsyncCallback<List<groups>> callback )
  {
    Backendless.Data.of( groups.class ).find( queryBuilder, callback );
  }
}
package com.whitepaper.tarko.data.sql;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.TypeConverters;

import java.util.List;

@Dao
@TypeConverters(Converters.class)
public interface DiscountDao {

    @Query("select * from discount")
    List<DiscountEntity> loadAllDiscountsByDate();

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    void insertDiscount(DiscountEntity discountEntity);

    @Delete
    void deleteDiscount(DiscountEntity discountEntity);

}

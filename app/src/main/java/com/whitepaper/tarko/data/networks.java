
package com.whitepaper.tarko.data;

import com.backendless.Backendless;
import com.backendless.async.callback.AsyncCallback;
import com.backendless.persistence.DataQueryBuilder;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

public class networks implements Serializable
{
  private String ownerId;
  private String objectId;
    private Date updated;
  private String title;
  private String image;
    private Date created;
    private List<stores> stores;
    private campaigns campaigns;

    public com.whitepaper.tarko.data.campaigns getCampaigns() {
        return campaigns;
    }

    public void setCampaigns(com.whitepaper.tarko.data.campaigns campaigns) {
        this.campaigns = campaigns;
    }

  public String getOwnerId()
  {
    return ownerId;
  }

  public String getObjectId()
  {
    return objectId;
  }

    public Date getUpdated()
  {
    return updated;
  }

  public String getTitle()
  {
    return title;
  }

  public void setTitle( String title )
  {
    this.title = title;
  }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public Date getCreated()
  {
    return created;
  }

    public List<stores> getStores() {
        return stores;
    }

    public void setStores(List<stores> stores) {
        this.stores = stores;
    }

                                                    
  public networks save()
  {
    return Backendless.Data.of( networks.class ).save( this );
  }

  public void saveAsync( AsyncCallback<networks> callback )
  {
    Backendless.Data.of( networks.class ).save( this, callback );
  }

  public Long remove()
  {
    return Backendless.Data.of( networks.class ).remove( this );
  }

  public void removeAsync( AsyncCallback<Long> callback )
  {
    Backendless.Data.of( networks.class ).remove( this, callback );
  }

  public static networks findById( String id )
  {
    return Backendless.Data.of( networks.class ).findById( id );
  }

  public static void findByIdAsync( String id, AsyncCallback<networks> callback )
  {
    Backendless.Data.of( networks.class ).findById( id, callback );
  }

  public static networks findFirst()
  {
    return Backendless.Data.of( networks.class ).findFirst();
  }

  public static void findFirstAsync( AsyncCallback<networks> callback )
  {
    Backendless.Data.of( networks.class ).findFirst( callback );
  }

  public static networks findLast()
  {
    return Backendless.Data.of( networks.class ).findLast();
  }

  public static void findLastAsync( AsyncCallback<networks> callback )
  {
    Backendless.Data.of( networks.class ).findLast( callback );
  }

  public static List<networks> find( DataQueryBuilder queryBuilder )
  {
    return Backendless.Data.of( networks.class ).find( queryBuilder );
  }

  public static void findAsync( DataQueryBuilder queryBuilder, AsyncCallback<List<networks>> callback )
  {
    Backendless.Data.of( networks.class ).find( queryBuilder, callback );
  }
}

package com.whitepaper.tarko.data;

import com.backendless.Backendless;
import com.backendless.async.callback.AsyncCallback;
import com.backendless.persistence.DataQueryBuilder;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

public class cities implements Serializable
{
  private String ownerId;
  private String ru_title;
  private Boolean is_available;
  private String en_title;
  private String title;
  private String objectId;
  private Date updated;
  private Date created;
  private Double longitude;
  private String et_title;
  private Double latitude;
  public String getOwnerId()
  {
    return ownerId;
  }

  public String getRu_title() {
    return ru_title;
  }

  public void setRu_title(String ru_title) {
    this.ru_title = ru_title;
  }

  public Boolean getIs_available() {
    return is_available;
  }

  public void setIs_available(Boolean is_available) {
    this.is_available = is_available;
  }

  public String getEn_title() {
    return en_title;
  }

  public void setEn_title(String en_title) {
    this.en_title = en_title;
  }

  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public String getObjectId()
  {
    return objectId;
  }

  public Date getUpdated()
  {
    return updated;
  }

  public Date getCreated()
  {
    return created;
  }

  public Double getLongitude() {
    return longitude;
  }

  public void setLongitude(Double longitude) {
    this.longitude = longitude;
  }

  public String getEt_title() {
    return et_title;
  }

  public void setEt_title(String et_title) {
    this.et_title = et_title;
  }

  public Double getLatitude() {
    return latitude;
  }

  public void setLatitude(Double latitude) {
    this.latitude = latitude;
  }

                                                    
  public cities save()
  {
    return Backendless.Data.of( cities.class ).save( this );
  }

  public void saveAsync( AsyncCallback<cities> callback )
  {
    Backendless.Data.of( cities.class ).save( this, callback );
  }

  public Long remove()
  {
    return Backendless.Data.of( cities.class ).remove( this );
  }

  public void removeAsync( AsyncCallback<Long> callback )
  {
    Backendless.Data.of( cities.class ).remove( this, callback );
  }

  public static cities findById( String id )
  {
    return Backendless.Data.of( cities.class ).findById( id );
  }

  public static void findByIdAsync( String id, AsyncCallback<cities> callback )
  {
    Backendless.Data.of( cities.class ).findById( id, callback );
  }

  public static cities findFirst()
  {
    return Backendless.Data.of( cities.class ).findFirst();
  }

  public static void findFirstAsync( AsyncCallback<cities> callback )
  {
    Backendless.Data.of( cities.class ).findFirst( callback );
  }

  public static cities findLast()
  {
    return Backendless.Data.of( cities.class ).findLast();
  }

  public static void findLastAsync( AsyncCallback<cities> callback )
  {
    Backendless.Data.of( cities.class ).findLast( callback );
  }

  public static List<cities> find( DataQueryBuilder queryBuilder )
  {
    return Backendless.Data.of( cities.class ).find( queryBuilder );
  }

  public static void findAsync( DataQueryBuilder queryBuilder, AsyncCallback<List<cities>> callback )
  {
    Backendless.Data.of( cities.class ).find( queryBuilder, callback );
  }
}

package com.whitepaper.tarko.data;

import com.backendless.Backendless;
import com.backendless.async.callback.AsyncCallback;
import com.backendless.persistence.DataQueryBuilder;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

public class stores implements Serializable
{
  private String objectId;
    private Date updated;
  private String ownerId;
  private String title;
    private String work_hours;
    private String address;
    private Date created;
    private Double latitude;
    private Double longitude;
  public String getObjectId()
  {
    return objectId;
  }

    public Date getUpdated()
  {
    return updated;
  }

  public String getOwnerId()
  {
    return ownerId;
  }

  public String getTitle()
  {
    return title;
  }

  public void setTitle( String title )
  {
    this.title = title;
  }

    public String getWork_hours() {
        return work_hours;
    }

    public void setWork_hours(String work_hours) {
        this.work_hours = work_hours;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Date getCreated()
  {
    return created;
  }

    public Double getLatitude() {
        return latitude;
  }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
  }

    public Double getLongitude() {
        return longitude;
  }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
  }

                                                    
  public stores save()
  {
    return Backendless.Data.of( stores.class ).save( this );
  }

  public void saveAsync( AsyncCallback<stores> callback )
  {
    Backendless.Data.of( stores.class ).save( this, callback );
  }

  public Long remove()
  {
    return Backendless.Data.of( stores.class ).remove( this );
  }

  public void removeAsync( AsyncCallback<Long> callback )
  {
    Backendless.Data.of( stores.class ).remove( this, callback );
  }

  public static stores findById( String id )
  {
    return Backendless.Data.of( stores.class ).findById( id );
  }

  public static void findByIdAsync( String id, AsyncCallback<stores> callback )
  {
    Backendless.Data.of( stores.class ).findById( id, callback );
  }

  public static stores findFirst()
  {
    return Backendless.Data.of( stores.class ).findFirst();
  }

  public static void findFirstAsync( AsyncCallback<stores> callback )
  {
    Backendless.Data.of( stores.class ).findFirst( callback );
  }

  public static stores findLast()
  {
    return Backendless.Data.of( stores.class ).findLast();
  }

  public static void findLastAsync( AsyncCallback<stores> callback )
  {
    Backendless.Data.of( stores.class ).findLast( callback );
  }

  public static List<stores> find( DataQueryBuilder queryBuilder )
  {
    return Backendless.Data.of( stores.class ).find( queryBuilder );
  }

  public static void findAsync( DataQueryBuilder queryBuilder, AsyncCallback<List<stores>> callback )
  {
    Backendless.Data.of( stores.class ).find( queryBuilder, callback );
  }
}
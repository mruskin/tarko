
package com.whitepaper.tarko.data;

import com.backendless.Backendless;
import com.backendless.async.callback.AsyncCallback;
import com.backendless.persistence.DataQueryBuilder;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

public class countries implements Serializable {
    private Date created;
    private String title;
  private String objectId;
    private String ru_title;
    private String en_title;
  private String ownerId;
    private Date updated;
    private String et_title;
  private List<cities> cities;

    public Date getCreated()
  {
    return created;
  }

    public String getTitle() {
        return title;
  }

    public void setTitle(String title) {
        this.title = title;
  }

  public String getObjectId()
  {
    return objectId;
  }

    public String getRu_title() {
        return ru_title;
    }

    public void setRu_title(String ru_title) {
        this.ru_title = ru_title;
  }

    public String getEn_title() {
        return en_title;
    }

    public void setEn_title(String en_title) {
        this.en_title = en_title;
  }

  public String getOwnerId()
  {
    return ownerId;
  }

    public Date getUpdated()
  {
    return updated;
  }

    public String getEt_title() {
        return et_title;
  }

    public void setEt_title(String et_title) {
        this.et_title = et_title;
  }

  public List<cities> getCities()
  {
    return cities;
  }

  public void setCities( List<cities> cities )
  {
    this.cities = cities;
  }

                                                    
  public countries save()
  {
    return Backendless.Data.of( countries.class ).save( this );
  }

  public void saveAsync( AsyncCallback<countries> callback )
  {
    Backendless.Data.of( countries.class ).save( this, callback );
  }

  public Long remove()
  {
    return Backendless.Data.of( countries.class ).remove( this );
  }

  public void removeAsync( AsyncCallback<Long> callback )
  {
    Backendless.Data.of( countries.class ).remove( this, callback );
  }

  public static countries findById( String id )
  {
    return Backendless.Data.of( countries.class ).findById( id );
  }

  public static void findByIdAsync( String id, AsyncCallback<countries> callback )
  {
    Backendless.Data.of( countries.class ).findById( id, callback );
  }

  public static countries findFirst()
  {
    return Backendless.Data.of( countries.class ).findFirst();
  }

  public static void findFirstAsync( AsyncCallback<countries> callback )
  {
    Backendless.Data.of( countries.class ).findFirst( callback );
  }

  public static countries findLast()
  {
    return Backendless.Data.of( countries.class ).findLast();
  }

  public static void findLastAsync( AsyncCallback<countries> callback )
  {
    Backendless.Data.of( countries.class ).findLast( callback );
  }

  public static List<countries> find( DataQueryBuilder queryBuilder )
  {
    return Backendless.Data.of( countries.class ).find( queryBuilder );
  }

  public static void findAsync( DataQueryBuilder queryBuilder, AsyncCallback<List<countries>> callback )
  {
    Backendless.Data.of( countries.class ).find( queryBuilder, callback );
  }
}
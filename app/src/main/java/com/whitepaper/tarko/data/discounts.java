package com.whitepaper.tarko.data;

import com.backendless.Backendless;
import com.backendless.async.callback.AsyncCallback;
import com.backendless.persistence.DataQueryBuilder;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

public class discounts implements Serializable
{
    private String ownerId;
    private Double discountPrice;
    private String image;
    private String objectId;
    private Date created;
    private Double weight;
    private Double regularPrice;
    private Date updated;
    private String et_title;
    private Double volume;
    private String en_title;
    private String ru_title;
    private String title;
    private groups group;
    private campaignsReduced campaign;


    public campaignsReduced getCampaign() {
        return campaign;
    }

    public void setCampaign(campaignsReduced campaign) {
        this.campaign = campaign;
  }

  public long getDiscountPercent() {
      if (getDiscountPrice() == null || getRegularPrice() == null) {
          return 0;
      } else {
          return Math.round((1 - getDiscountPrice() / getRegularPrice()) * 100);
      }
  }

    public String getOwnerId() {
        return ownerId;
  }

  public Double getDiscountPrice()
  {
    return discountPrice;
  }

  public void setDiscountPrice( Double discountPrice )
  {
    this.discountPrice = discountPrice;
  }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

  public String getObjectId()
  {
    return objectId;
  }

    public Date getCreated()
  {
    return created;
  }

    public Double getWeight() {
        return weight;
    }

    public void setWeight(Double weight) {
        this.weight = weight;
    }

    public Double getRegularPrice() {
        return regularPrice;
    }

    public void setRegularPrice(Double regularPrice) {
        this.regularPrice = regularPrice;
    }

    public Date getUpdated() {
        return updated;
    }

    public String getEt_title() {
        return et_title;
  }

    public void setEt_title(String et_title) {
        this.et_title = et_title;
  }

    public Double getVolume() {
        return volume;
  }

    public void setVolume(Double volume) {
        this.volume = volume;
    }

    public String getEn_title() {
        return en_title;
    }

    public void setEn_title(String en_title) {
        this.en_title = en_title;
    }

    public String getRu_title() {
        return ru_title;
    }

    public void setRu_title(String ru_title) {
        this.ru_title = ru_title;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public groups getGroup() {
        return group;
    }

    public void setGroup(groups group) {
        this.group = group;
  }

                                                    
  public discounts save()
  {
    return Backendless.Data.of( discounts.class ).save( this );
  }

  public void saveAsync( AsyncCallback<discounts> callback )
  {
    Backendless.Data.of( discounts.class ).save( this, callback );
  }

  public Long remove()
  {
    return Backendless.Data.of( discounts.class ).remove( this );
  }

  public void removeAsync( AsyncCallback<Long> callback )
  {
    Backendless.Data.of( discounts.class ).remove( this, callback );
  }

  public static discounts findById( String id )
  {
    return Backendless.Data.of( discounts.class ).findById( id );
  }

  public static void findByIdAsync( String id, AsyncCallback<discounts> callback )
  {
    Backendless.Data.of( discounts.class ).findById( id, callback );
  }

  public static discounts findFirst()
  {
    return Backendless.Data.of( discounts.class ).findFirst();
  }

  public static void findFirstAsync( AsyncCallback<discounts> callback )
  {
    Backendless.Data.of( discounts.class ).findFirst( callback );
  }

  public static discounts findLast()
  {
    return Backendless.Data.of( discounts.class ).findLast();
  }

  public static void findLastAsync( AsyncCallback<discounts> callback )
  {
    Backendless.Data.of( discounts.class ).findLast( callback );
  }

  public static List<discounts> find( DataQueryBuilder queryBuilder )
  {
    return Backendless.Data.of( discounts.class ).find( queryBuilder );
  }

  public static void findAsync( DataQueryBuilder queryBuilder, AsyncCallback<List<discounts>> callback )
  {
    Backendless.Data.of( discounts.class ).find( queryBuilder, callback );
  }
}
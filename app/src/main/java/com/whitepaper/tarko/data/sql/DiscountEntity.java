package com.whitepaper.tarko.data.sql;

import androidx.annotation.NonNull;
import androidx.room.Entity;
import androidx.room.PrimaryKey;
import androidx.room.TypeConverters;

import java.io.Serializable;
import java.util.Date;

@Entity(tableName = "discount")
@TypeConverters(Converters.class)
public class DiscountEntity implements Serializable {

    @PrimaryKey
    @NonNull
    private String objectId;
    private String ownerId;
    private Double discountPrice;
    private String image;
    private Date created;
    private Double weight;
    private Double regularPrice;
    private Date updated;
    private String et_title;
    private Double volume;
    private String en_title;
    private String ru_title;
    private String title;
    private String groupTitle;
    private String groupRuitle;
    private String groupEtTitle;
    private String networkImage;

    //    private groups group;
    private Date start_date;
    private Date end_date;
    private String networkTitle;
    private String campaignTitle;
    private String campaignEtTitle;
    private String campaignRuTitle;

    public String getGroupRuitle() {
        return groupRuitle;
    }

    public void setGroupRuitle(String groupRuitle) {
        this.groupRuitle = groupRuitle;
    }

    public String getGroupEtTitle() {
        return groupEtTitle;
    }

    public void setGroupEtTitle(String groupEtTitle) {
        this.groupEtTitle = groupEtTitle;
    }

    public String getCampaignEtTitle() {
        return campaignEtTitle;
    }

    public void setCampaignEtTitle(String campaignEtTitle) {
        this.campaignEtTitle = campaignEtTitle;
    }

    public String getCampaignRuTitle() {
        return campaignRuTitle;
    }

    public void setCampaignRuTitle(String campaignRuTitle) {
        this.campaignRuTitle = campaignRuTitle;
    }

    public String getNetworkImage() {
        return networkImage;
    }

    public void setNetworkImage(String networkImage) {
        this.networkImage = networkImage;
    }

    public String getGroupTitle() {
        return groupTitle;
    }

    public void setGroupTitle(String groupTitle) {
        this.groupTitle = groupTitle;
    }

    public String getOwnerId() {
        return ownerId;
    }

    public void setOwnerId(String ownerId) {
        this.ownerId = ownerId;
    }

    public Double getDiscountPrice() {
        return discountPrice;
    }

    public void setDiscountPrice(Double discountPrice) {
        this.discountPrice = discountPrice;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getObjectId() {
        return objectId;
    }

    public void setObjectId(String objectId) {
        this.objectId = objectId;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public Double getWeight() {
        return weight;
    }

    public void setWeight(Double weight) {
        this.weight = weight;
    }

    public Double getRegularPrice() {
        return regularPrice;
    }

    public void setRegularPrice(Double regularPrice) {
        this.regularPrice = regularPrice;
    }

    public Date getUpdated() {
        return updated;
    }

    public void setUpdated(Date updated) {
        this.updated = updated;
    }

    public String getEt_title() {
        return et_title;
    }

    public void setEt_title(String et_title) {
        this.et_title = et_title;
    }

    public Double getVolume() {
        return volume;
    }

    public void setVolume(Double volume) {
        this.volume = volume;
    }

    public String getEn_title() {
        return en_title;
    }

    public void setEn_title(String en_title) {
        this.en_title = en_title;
    }

    public String getRu_title() {
        return ru_title;
    }

    public void setRu_title(String ru_title) {
        this.ru_title = ru_title;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

//    public groups getGroup() {
//        return group;
//    }
//
//    public void setGroup(groups group) {
//        this.group = group;
//    }

    public Date getStart_date() {
        return start_date;
    }

    public void setStart_date(Date start_date) {
        this.start_date = start_date;
    }

    public Date getEnd_date() {
        return end_date;
    }

    public void setEnd_date(Date end_date) {
        this.end_date = end_date;
    }

    public String getNetworkTitle() {
        return networkTitle;
    }

    public void setNetworkTitle(String networkTitle) {
        this.networkTitle = networkTitle;
    }

    public String getCampaignTitle() {
        return campaignTitle;
    }

    public void setCampaignTitle(String campaignTitle) {
        this.campaignTitle = campaignTitle;
    }

    public long getDiscountPercent() {
        if (getDiscountPrice() == null || getRegularPrice() == null) {
            return 0;
        } else {
            return Math.round((1 - getDiscountPrice() / getRegularPrice()) * 100);
        }
    }
}

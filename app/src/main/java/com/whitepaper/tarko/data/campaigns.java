
package com.whitepaper.tarko.data;

import com.backendless.Backendless;
import com.backendless.async.callback.AsyncCallback;
import com.backendless.persistence.DataQueryBuilder;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

public class campaigns implements Serializable
{
  private String objectId;
  private String en_title;
  private Date start_date;
  private Date updated;
  private Date end_date;
  private String title;
  private Date created;
  private String ownerId;
  private String et_title;
  private String ru_title;
  private networks network;
  private List<discounts> discounts;
  private cities city;
  public String getObjectId()
  {
    return objectId;
  }

  public String getEn_title() {
    return en_title;
  }

  public void setEn_title(String en_title) {
    this.en_title = en_title;
  }

  public Date getStart_date()
  {
    return start_date;
  }

  public void setStart_date(Date start_date)
  {
    this.start_date = start_date;
  }

  public Date getUpdated()
  {
    return updated;
  }

  public Date getEnd_date()
  {
    return end_date;
  }

  public void setEnd_date(Date end_date)
  {
    this.end_date = end_date;
  }

  public String getTitle()
  {
    return title;
  }

  public void setTitle( String title )
  {
    this.title = title;
  }

  public Date getCreated()
  {
    return created;
  }

  public String getOwnerId()
  {
    return ownerId;
  }

  public String getEt_title() {
    return et_title;
  }

  public void setEt_title(String et_title) {
    this.et_title = et_title;
  }

  public String getRu_title() {
    return ru_title;
  }

  public void setRu_title(String ru_title) {
    this.ru_title = ru_title;
  }

  public networks getNetwork()
  {
    return network;
  }

  public void setNetwork( networks network )
  {
    this.network = network;
  }

  public List<discounts> getDiscounts() {
    return discounts;
  }

  public void setDiscounts(List<discounts> discounts) {
    this.discounts = discounts;
  }

  public cities getCity()
  {
    return city;
  }

  public void setCity( cities city )
  {
    this.city = city;
  }

                                                    
  public campaigns save()
  {
    return Backendless.Data.of( campaigns.class ).save( this );
  }

  public void saveAsync( AsyncCallback<campaigns> callback )
  {
    Backendless.Data.of( campaigns.class ).save( this, callback );
  }

  public Long remove()
  {
    return Backendless.Data.of( campaigns.class ).remove( this );
  }

  public void removeAsync( AsyncCallback<Long> callback )
  {
    Backendless.Data.of( campaigns.class ).remove( this, callback );
  }

  public static campaigns findById( String id )
  {
    return Backendless.Data.of( campaigns.class ).findById( id );
  }

  public static void findByIdAsync( String id, AsyncCallback<campaigns> callback )
  {
    Backendless.Data.of( campaigns.class ).findById( id, callback );
  }

  public static campaigns findFirst()
  {
    return Backendless.Data.of( campaigns.class ).findFirst();
  }

  public static void findFirstAsync( AsyncCallback<campaigns> callback )
  {
    Backendless.Data.of( campaigns.class ).findFirst( callback );
  }

  public static campaigns findLast()
  {
    return Backendless.Data.of( campaigns.class ).findLast();
  }

  public static void findLastAsync( AsyncCallback<campaigns> callback )
  {
    Backendless.Data.of( campaigns.class ).findLast( callback );
  }

  public static List<campaigns> find( DataQueryBuilder queryBuilder )
  {
    return Backendless.Data.of( campaigns.class ).find( queryBuilder );
  }

  public static void findAsync( DataQueryBuilder queryBuilder, AsyncCallback<List<campaigns>> callback )
  {
    Backendless.Data.of( campaigns.class ).find( queryBuilder, callback );
  }
}
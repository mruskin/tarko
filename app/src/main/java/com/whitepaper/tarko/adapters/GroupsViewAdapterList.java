package com.whitepaper.tarko.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.RequestCreator;
import com.whitepaper.tarko.R;
import com.whitepaper.tarko.config.Constants;
import com.whitepaper.tarko.config.LocaleHelper;
import com.whitepaper.tarko.data.groups;

import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Locale;

public class GroupsViewAdapterList extends RecyclerView.Adapter<GroupsViewAdapterList.MyViewHolder> {

    private LayoutInflater layoutInflater;

    private List<groups> dataSet;
    private OnGroupsListener onGroupsListener;
    private Format formatter = new SimpleDateFormat("EEE, dd MMM");
    private String lang;

    public GroupsViewAdapterList(Context context, List<groups> data, OnGroupsListener onGroupsListener) {
        this.dataSet = data;
        this.layoutInflater = LayoutInflater.from(context);
        this.onGroupsListener = onGroupsListener;
        this.lang = LocaleHelper.getPersistedData(context, Locale.getDefault().getLanguage());
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        TextView title;
        ImageView imageViewIcon;
        OnGroupsListener onGroupsClick;

        public MyViewHolder(View itemView, OnGroupsListener onGroupsClick) {
            super(itemView);
            this.imageViewIcon = itemView.findViewById(R.id.group_img_id);
            this.title = itemView.findViewById(R.id.group_title_id);
            this.onGroupsClick = onGroupsClick;
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            onGroupsClick.onGroupsClick(getAdapterPosition());
        }
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = layoutInflater.inflate(R.layout.groups_data, parent, false);
        MyViewHolder myViewHolder = new MyViewHolder(view, onGroupsListener);

        return myViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int listPosition) {
        TextView titleHolder = holder.title;
        ImageView imageView = holder.imageViewIcon;


        if (Constants.ESTONIAN_LANGUAGE.equals(lang)) {
            titleHolder.setText(dataSet.get(listPosition).getEt_title());
        } else if (Constants.RUSSIAN_LANGUAGE.equals(lang)) {
            titleHolder.setText(dataSet.get(listPosition).getRu_title());
        } else {
            titleHolder.setText(dataSet.get(listPosition).getTitle());
        }

        if (dataSet.get(listPosition).getImage() == null) {
            imageView.setImageResource(R.drawable.image_not_found);
        } else {
            try {
                RequestCreator requestCreator = Picasso.get().load(dataSet.get(listPosition).getImage());
                requestCreator.tag("TagName");
                requestCreator.memoryPolicy(MemoryPolicy.NO_CACHE);
                requestCreator.networkPolicy(NetworkPolicy.NO_STORE);
                requestCreator.into(imageView);
//                Picasso.get().load(dataSet.get(listPosition).getImage()).into(imageView);
            } catch (Exception ex) {
                Log.e("Image reading error: ", ex.getMessage());
            }

        }
    }

    @Override
    public int getItemCount() {
        return dataSet == null ? 0 : dataSet.size();
    }

    public interface OnGroupsListener {
        void onGroupsClick(int position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

}

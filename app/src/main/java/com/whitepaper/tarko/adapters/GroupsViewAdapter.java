package com.whitepaper.tarko.adapters;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.whitepaper.tarko.R;
import com.whitepaper.tarko.config.Constants;
import com.whitepaper.tarko.config.LocaleHelper;
import com.whitepaper.tarko.data.groups;

import java.util.List;
import java.util.Locale;

public class GroupsViewAdapter extends RecyclerView.Adapter<GroupsViewAdapter.MyViewHolder> {

    private Context mContext;
    private List<groups> mData;
    private OnGroupsListener onGroupsListener;
    private String lang;

    public GroupsViewAdapter(Context mContext, List<groups> mData, OnGroupsListener onGroupsListener) {
        this.mContext = mContext;
        this.mData = mData;
        this.onGroupsListener = onGroupsListener;
        this.lang = LocaleHelper.getPersistedData(mContext, Locale.getDefault().getLanguage());
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view;
        LayoutInflater mInflater = LayoutInflater.from(mContext);
        view = mInflater.inflate(R.layout.fragment_groups, parent, false);
        return new MyViewHolder(view, onGroupsListener);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        if (Constants.ESTONIAN_LANGUAGE.equals(lang)) {
            holder.tv_book_title.setText(mData.get(position).getEt_title());
        } else if (Constants.RUSSIAN_LANGUAGE.equals(lang)) {
            holder.tv_book_title.setText(mData.get(position).getRu_title());
        } else {
            holder.tv_book_title.setText(mData.get(position).getTitle());
        }

        if (mData.get(position).getImage() == null) {
            holder.img_book_thumbnail.setImageResource(R.drawable.image_not_found);
        } else {
            Picasso.get().load(mData.get(position).getImage()).into(holder.img_book_thumbnail);
        }
    }

    @Override
    public int getItemCount() {
        return mData == null ? 0 : mData.size();
    }

    public interface OnGroupsListener {
        void onGroupsClick(int position);
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        TextView tv_book_title;
        ImageView img_book_thumbnail;
        CardView cardView;
        OnGroupsListener onGroupsListener;

        public MyViewHolder(View itemView, OnGroupsListener onGroupsListener) {
            super(itemView);

            tv_book_title = itemView.findViewById(R.id.group_title_id);
            img_book_thumbnail = itemView.findViewById(R.id.group_img_id);
            cardView = itemView.findViewById(R.id.groupsCardview_id);
            this.onGroupsListener = onGroupsListener;
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            onGroupsListener.onGroupsClick(getAdapterPosition());
        }
    }
}

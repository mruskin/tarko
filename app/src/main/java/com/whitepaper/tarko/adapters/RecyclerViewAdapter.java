package com.whitepaper.tarko.adapters;

import android.content.Context;
import android.graphics.Paint;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.whitepaper.tarko.R;
import com.whitepaper.tarko.config.Constants;
import com.whitepaper.tarko.config.LocaleHelper;
import com.whitepaper.tarko.data.sql.DiscountEntity;

import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Locale;

public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerViewAdapter.MyViewHolder> {

    private LayoutInflater layoutInflater;

    private List<DiscountEntity> dataSet;
    private OnGoodsListener onGoodsListener;
    private String lang;
    private Format formatter = new SimpleDateFormat("EEE, dd MMM");

    public RecyclerViewAdapter(Context context, List<DiscountEntity> data, OnGoodsListener onGoodsListener) {
        this.dataSet = data;
        this.layoutInflater = LayoutInflater.from(context);
        this.onGoodsListener = onGoodsListener;
        this.lang = LocaleHelper.getPersistedData(context, Locale.getDefault().getLanguage());
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        TextView textViewTitle;
        TextView textViewCampaignPeriod;
        TextView textViewDiscountPercent;
        TextView textViewOldPrice;
        TextView textViewNewPrice;
        TextView euro1;
        TextView euro2;
        TextView percent;
        ImageView imageViewIcon;
        ImageView imageShop;
        OnGoodsListener onGoodsListener;

        public MyViewHolder(View itemView, OnGoodsListener onGoodsListener) {
            super(itemView);
            this.textViewTitle = itemView.findViewById(R.id.goodTitle);
            this.textViewCampaignPeriod = itemView.findViewById(R.id.campaignPeriod);
            this.textViewDiscountPercent = itemView.findViewById(R.id.discountPercent);
            this.textViewOldPrice = itemView.findViewById(R.id.oldPrice);
            this.textViewNewPrice = itemView.findViewById(R.id.newPrice);
            this.imageViewIcon = itemView.findViewById(R.id.imageView);
            this.imageShop = itemView.findViewById(R.id.shopImage);
            this.euro1 = itemView.findViewById(R.id.eurosign1);
            this.euro2 = itemView.findViewById(R.id.eurosign2);
            this.percent = itemView.findViewById(R.id.percentSign);
            this.onGoodsListener = onGoodsListener;
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            onGoodsListener.onGoodsClick(getAdapterPosition());
        }
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = layoutInflater.inflate(R.layout.cards_layout, parent, false);
        MyViewHolder myViewHolder = new MyViewHolder(view, onGoodsListener);

        return myViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int listPosition) {
        TextView textViewTitle = holder.textViewTitle;
        TextView textViewCampaignPeriod = holder.textViewCampaignPeriod;
        TextView textViewDiscountPercent = holder.textViewDiscountPercent;
        TextView textViewOldPrice = holder.textViewOldPrice;
        TextView textViewNewPrice = holder.textViewNewPrice;
        TextView euro1 = holder.euro1;
        TextView euro2 = holder.euro2;
        TextView percent = holder.percent;
        ImageView imageView = holder.imageViewIcon;
        ImageView shopimage = holder.imageShop;


        String startDate = formatter.format(dataSet.get(listPosition).getStart_date());
        String endDate = formatter.format(dataSet.get(listPosition).getEnd_date());

        euro1.setText(Constants.EURO);
        euro2.setText(Constants.EURO);
        percent.setText("%");

        if (Constants.ESTONIAN_LANGUAGE.equals(lang)) {
            textViewTitle.setText(dataSet.get(listPosition).getEt_title());
        } else if (Constants.RUSSIAN_LANGUAGE.equals(lang)) {
            textViewTitle.setText(dataSet.get(listPosition).getRu_title());
        } else {
            textViewTitle.setText(dataSet.get(listPosition).getTitle());
        }

        textViewDiscountPercent.setText("-" + dataSet.get(listPosition).getDiscountPercent());
        textViewOldPrice.setText(dataSet.get(listPosition).getRegularPrice() + "");
        textViewOldPrice.setPaintFlags(textViewOldPrice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
        textViewNewPrice.setText(dataSet.get(listPosition).getDiscountPrice() + "");
        textViewCampaignPeriod.setText(startDate + " - " + endDate);
        Picasso.get().load(dataSet.get(listPosition).getNetworkImage()).into(shopimage);
        if (dataSet.get(listPosition).getImage() == null) {
            imageView.setImageResource(R.drawable.image_not_found);
        } else {
            Picasso.get().load(dataSet.get(listPosition).getImage()).into(imageView);
        }
    }

    @Override
    public int getItemCount() {
        return dataSet == null ? 0 : dataSet.size();
    }

    public interface OnGoodsListener{
        void onGoodsClick(int position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

}

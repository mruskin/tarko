package com.whitepaper.tarko.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.whitepaper.tarko.R;
import com.whitepaper.tarko.data.networks;

import java.util.List;

public class NetworksViewAdapter extends RecyclerView.Adapter<NetworksViewAdapter.MyViewHolder> {

    private Context mContext;
    private List<networks> mData;
    private OnNetworksListener onNetworksListener;

    public NetworksViewAdapter(Context mContext, List<networks> mData, OnNetworksListener onNetworksListener) {
        this.mContext = mContext;
        this.mData = mData;
        this.onNetworksListener = onNetworksListener;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view;
        LayoutInflater mInflater = LayoutInflater.from(mContext);
        view = mInflater.inflate(R.layout.fragment_networks, parent, false);
        return new MyViewHolder(view, onNetworksListener);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {

        holder.tv_book_title.setText(mData.get(position).getTitle());
        Picasso.get().load(mData.get(position).getImage()).into(holder.img_book_thumbnail);
//        holder.cardView.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//                Intent intent = new Intent(mContext,Book_Activity.class);
//
//                // passing data to the book activity
//                intent.putExtra("Title",mData.get(position).getTitle());
//                intent.putExtra("Description",mData.get(position).getDescription());
//                intent.putExtra("Thumbnail",R.drawable.ic_assignment_black_24dp);
//                // start the activity
//                mContext.startActivity(intent);
//
//            }
//        });


    }

    @Override
    public int getItemCount() {
        return mData == null ? 0 : mData.size();
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        TextView tv_book_title;
        ImageView img_book_thumbnail;
        CardView cardView;
        OnNetworksListener onNetworksListener;

        private MyViewHolder(View itemView, OnNetworksListener onNetworksListener) {
            super(itemView);

            tv_book_title = (TextView) itemView.findViewById(R.id.store_title_id);
            img_book_thumbnail = (ImageView) itemView.findViewById(R.id.store_img_id);
            cardView = (CardView) itemView.findViewById(R.id.cardview_id);
            this.onNetworksListener = onNetworksListener;
            itemView.setOnClickListener(this);

        }

        @Override
        public void onClick(View v) {
            onNetworksListener.onNetworksClick(getAdapterPosition());
        }
    }

    public interface OnNetworksListener {
        void onNetworksClick(int position);
    }
}
